//Student package
//Consist of struct definitions, constructor & Repository interface
package Student

import (
	"regexp"
	"strconv"
)

type Student struct {
	ID   int64  `json:"id"`
	Age  int64  `json:"age"`
	Name string `json:"name"`
}

type IStudentDAO interface {
	GetStudentByID(int64) (*Student, error)
	GetAllStudents() ([]*Student, error)
	AddStudent(*Student) (int64, error)
	UpdateStudentByID(int64, *Student) error
	DeleteStudentByID(int64) error
}

func New(id int64, name string, age int64) *Student {
	return &Student{
		ID:   id,
		Name: name,
		Age:  age,
	}
}

func (s Student) ValidStudent() bool {
	idOk, _ := regexp.MatchString("^[0-9]+$", strconv.Itoa(int(s.ID)))
	nameOk, _ := regexp.MatchString("^[a-zA-Z]{1,24}$", s.Name)
	ageOk, _ := regexp.MatchString("^[0-9]{1,3}$", strconv.Itoa(int(s.Age)))

	return idOk && nameOk && ageOk && s.ID != 0
}
