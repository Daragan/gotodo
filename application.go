package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"

	_ "github.com/denisenkom/go-mssqldb"
	"github.com/gorilla/mux"

	"./Student"
	"./StudentDAO"
)

const (
	API_VERSION = "v1"
	SERVER_PORT = 5000
)

var (
	password string
	port     int = 5432
	server   string
	user     string
	db       Student.IStudentDAO
)

func main() {

	//For gracefully exit
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		for sig := range c {
			if sig == os.Interrupt {
				StudentDAO.CloseConnection()
				close(c)
				log.Println("Server stopped!")
				os.Exit(0)
			}
		}
	}()

	log.Println("Getting env args...")
	server = os.Getenv("server")
	password = os.Getenv("password")
	user = os.Getenv("user")
	log.Println("Finished!")

	log.Println("Init connection to MS SQL DB...")
	db, _ = StudentDAO.InitDB(server, port, user, password)
	defer StudentDAO.CloseConnection()
	log.Println("Finished!")

	router := mux.NewRouter()
	router.HandleFunc("/", indexHandler)
	router.HandleFunc("/api/"+API_VERSION+"/student", actStudentHandler)
	router.HandleFunc("/api/"+API_VERSION+"/student/{id:[0-9]+}", actStudentByIdHandler)
	router.HandleFunc("/health",
		func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprint(w, "OK")
		})

	log.Println("Server started!")
	http.ListenAndServe(":"+strconv.Itoa(SERVER_PORT), router)
}

func indexHandler(w http.ResponseWriter, _ *http.Request) {
	w.Write([]byte("Try curl to /api/" + API_VERSION + "/student"))
}

func actStudentHandler(w http.ResponseWriter, r *http.Request) {

	log.Println(r.Method, " ", r.URL.Path)
	w.Header().Set("Content-Type", "application/json")

	//Get all Students
	if r.Method == "GET" {
		students, err := db.GetAllStudents()
		if err != nil {
			log.Printf("Cannot get all Students. Error: %v", err.Error())
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		bs, _ := json.Marshal(students)
		log.Printf("Got all Students: %v", string(bs))
		w.Write(bs)
		return
	}

	//Add new Student
	if r.Method == "POST" {

		decoder := json.NewDecoder(r.Body)
		var st Student.Student
		err := decoder.Decode(&st)
		if err != nil {
			log.Println("Cannot decode JSON from request! Error: ", err.Error())
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		defer r.Body.Close()

		if st.ValidStudent() != true {
			log.Printf("Student is not valid! %v", st)
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		st.ID, err = db.AddStudent(&st)
		if err != nil {
			log.Printf("Cannot exec AddStudent()! Error: %v", err.Error())
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		log.Printf("Add new Student: %v\n", st)
		w.WriteHeader(http.StatusOK)
		return
	}
}

func actStudentByIdHandler(w http.ResponseWriter, r *http.Request) {
	log.Println(r.Method, " ", r.URL.Path)

	vars := mux.Vars(r)
	w.Header().Set("Content-Type", "application/json")
	id, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		log.Println("Cannot parse id. Error: ", err.Error())
		return
	}

	//GET by ID
	if r.Method == "GET" {
		st, err := db.GetStudentByID(id)
		if err != nil {
			log.Println("Error: ", err.Error())
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		bs, err := json.Marshal(*st)
		if err != nil {
			log.Println("Error: ", err.Error())
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		if st.ID == 0 {
			log.Printf("Student doesn`t exist!\n")
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		log.Printf("GET Student %v\n", *st)
		w.Write(bs)
		return
	}

	//DELETE by ID
	if r.Method == "DELETE" {

		err := db.DeleteStudentByID(id)
		if err != nil {
			log.Println("Error: ", err.Error())
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		log.Printf("DELETE Student with ID = %v\n", vars["id"])
		w.WriteHeader(http.StatusOK)
	}

	//UPDATE by ID
	if r.Method == "PUT" {
		decoder := json.NewDecoder(r.Body)
		defer r.Body.Close()

		var st Student.Student

		err := decoder.Decode(&st)
		if err != nil {
			log.Println("Cannot decode JSON from request! Error: ", err.Error())
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		if st.ValidStudent() != true {
			log.Printf("Student is not valid! %v", st)
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		err = db.UpdateStudentByID(id, Student.New(id, st.Name, st.Age))
		if err != nil {
			log.Println("Error: ", err.Error())
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		log.Printf("UPDATE Student with ID = %v\n", vars["id"])
		w.WriteHeader(http.StatusOK)
		return
	}
}
