# README #

Student-service with REST API

## Usage ##
1. Get all Students
`curl -i /api/v1/student`
2. Get the Student by ID
`curl -i /api/v1/student/<ID>`
3. Add a new Student
`curl -i -XPOST /api/v1/student -d '{"name":"name", "id":1, "age":11}'`
4. Update an existing Student
`curl -i -XPUT /api/v1/student/<id> -d '{"name":"newName", "id":1, "age":21}'`
5. Delete an existing Student
`curl -i -XDELETE /api/v1/student/<id>`

## DEMO ##
[Go to demo in AWS](http://gostudent-env.kr2xgxiz62.eu-central-1.elasticbeanstalk.com)