//StudentDAO package
//Consist of MS SQL connection initializing &
//implementation of Repository interface from Student package
package StudentDAO

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/denisenkom/go-mssqldb"

	st "../Student"
)

var db MsDB

type MsDB struct {
	connection *sql.DB
}

func InitDB(server string, port int, user string, password string) (d *MsDB, err error) {
	connString := fmt.Sprintf("server=%s;user id=%s;password=%s;port=%d",
		server, user, password, port)

	db.connection, _ = sql.Open("mssql", connString)
	err = db.connection.Ping()
	if err != nil {
		log.Fatalln("Cannot initialize DB connection! ", err.Error())
		return &MsDB{}, err
	}

	return &db, nil
}

func (db *MsDB) GetAllStudents() (s []*st.Student, err error) {
	var id, age int64
	var name string
	s = make([]*st.Student, 0)

	rows, err := db.connection.Query("select * FROM golang.go.Students")
	defer rows.Close()
	if err != nil {
		log.Println("Cannot execute query! ", err.Error())
		return
	}

	for rows.Next() {
		err = rows.Scan(&id, &name, &age)
		if err != nil {
			log.Println("Cannot read row! ", err.Error())
			return
		}

		s = append(s, st.New(id, name, age))
	}
	return
}

func (db *MsDB) GetStudentByID(id int64) (*st.Student, error) {

	var age int64
	var name string

	row := db.connection.QueryRow("SELECT name, age FROM golang.go.Students WHERE id = ?1", id)
	err := row.Scan(&name, &age)

	if err != nil {
		log.Println("Cannot read row! ", err.Error())
		return st.New(0, "", 0), err
	}

	return st.New(id, name, age), nil
}

func (db *MsDB) AddStudent(s *st.Student) (int64, error) {

	res, err := db.connection.Exec("INSERT INTO golang.go.Students (name, age) VALUES (?1, ?2)", s.Name, s.Age)
	newId, _ := res.LastInsertId()

	if err != nil {
		log.Println("Cannot insert row! ", err.Error())
		return 0, err
	}

	return newId, err
}

func (db *MsDB) UpdateStudentByID(id int64, new *st.Student) error {
	var age int64
	var name string

	row := db.connection.QueryRow("SELECT name, age FROM golang.go.Students WHERE id = ?1", id)
	err := row.Scan(&name, &age)

	if err != nil {
		log.Println("Cannot read row! ", err.Error())
		return err
	}

	_, err = db.connection.Exec("UPDATE golang.go.Students SET name = ?1, age = ?2 WHERE id = ?3;",
		new.Name, new.Age, id)
	if err != nil {
		log.Println("Cannot update row! ", err.Error())
		return err
	}

	return err
}

func (db *MsDB) DeleteStudentByID(id int64) error {

	_, err := db.connection.Exec("BEGIN TRANSACTION; DELETE FROM golang.go.Students WHERE id = ?1; COMMIT", id)

	if err != nil {
		log.Println("Cannot execute query! ", err.Error())
		return err
	}

	log.Printf("Student with ID = %d is deleted!\n", id)
	return err
}

func CloseConnection() {
	db.connection.Close()
	log.Println("DB connection is closed!")
}
